import { Component, ViewChild } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, NgForm } from '@angular/forms';
import { PedidosService } from '../cliente-create/pedidos.service';
import { CommonModule } from '@angular/common';
import Swal from "sweetalert2"
import { Router } from '@angular/router';

@Component({
  selector: 'app-cliente-create',
  standalone: true,
  imports: [
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    CommonModule,
  ],
  templateUrl: './cliente-create.component.html',
  styleUrl: './cliente-create.component.scss'
})
export class ClienteCreateComponent {
  @ViewChild('pedidoForm') pedidoForm!: NgForm;

  pedidos: any = {
    id_cliente: '',
    fecha_pedido: '',
    total: ''
  }

  data: any

  constructor(private pedidosSerice: PedidosService, private router: Router) { }


  altaPedidos() {
    Swal.fire({
      title: "Deseas crear el pedido?",
      showDenyButton: true,
      confirmButtonText: "Crear",
      denyButtonText: `Salir`
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        let formData = new FormData();
        formData.append('id_cliente', this.pedidos.id_cliente);
        formData.append('fecha_pedido', this.pedidos.fecha_pedido);
        formData.append('total', this.pedidos.total);

        this.pedidosSerice.postMethod('ventas/crearPedido.php', formData)
          .subscribe((res: any) => {
            if (res = "success") {
              Swal.fire("Pedido creado", "", "success");
              console.log(res);
              this.router.navigate(['/cliente-list']);
            }
          })
      } else if (result.isDenied) {
        Swal.fire("Algo ha salido mal\nNo se guardaron los cambios", "", "info");
      }
    });




  }
}
