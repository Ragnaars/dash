import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableModule, MatTableDataSource } from '@angular/material/table';
import { PedidosService } from './pedidos.service';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatIcon } from '@angular/material/icon';
import { RouterOutlet, RouterModule } from '@angular/router';
import Swal from "sweetalert2"


export interface PedidoElement {
  id_pedido: number;
  id_cliente: number;
  fecha_pedido: string;
  total: number;
}

const ELEMENT_DATA: PedidoElement[] = [];

@Component({
  selector: 'app-cliente-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatPaginatorModule,
    MatIcon,
    RouterOutlet,
    RouterModule],
  templateUrl: './cliente-list.component.html',
  styleUrl: './cliente-list.component.scss'
})
export class ClienteListComponent implements OnInit {
  displayedColumns: string[] = ['id_pedido', 'id_cliente', 'fecha_pedido', 'total', 'editar', 'eliminar'];

  dataSource = new MatTableDataSource<PedidoElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private pedidosService: PedidosService) {

  }

  deletePedido(id_pedido: any) {
    Swal.fire({
      title: "¿Eliminar pedido?",
      showCancelButton: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: `Cancelar`,
      cancelButtonColor: '#d33'
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {

        this.pedidosService.postMethod('ventas/deletePedido.php', { id_pedido: id_pedido })
          .subscribe(resp => {
            Swal.fire("Eliminado correctamente!", "", "success");
            this.getPedidos();
          });
      }
    });

  }

  ngOnInit() {
    this.getPedidos();
  }

  getPedidos() {
    this.pedidosService.getMethod('ventas/getPedidos.php')
      .subscribe(resp => {
        this.dataSource = new MatTableDataSource<PedidoElement>(resp.document);
        this.dataSource.paginator = this.paginator;
      });
  }

}
