import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js/auto';
import { ChartService } from './chart.service';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { provideNativeDateAdapter } from '@angular/material/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { ClienteListComponent } from '../cliente-list/cliente-list.component';




@Component({
  selector: 'app-chart',
  standalone: true,
  imports: [
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    CommonModule,
    ClienteListComponent
  ],
  providers: [provideNativeDateAdapter()],
  templateUrl: './chart.component.html',
  styleUrl: './chart.component.scss'
})
export class ChartComponent implements OnInit {

  data: any;
  dataidcliente: any[] = [];
  datacantidadpedidos: any[] = [];
  dataidpedido: any[] = [];
  datafechapedido: any[] = [];
  datatotal: any[] = [];
  dataPromGasto: any[] = [];
  indicators = [
    {
      title: 'INCIDENTES AMBIENTALES MAYORES',
      color: 'alert-danger',
      icon: 'fa fa-hospital-o',
      value: 0,
      class: 'col-md-4 col-sm-6'
    },
    {
      title: 'INCIDENTES AMBIENTABLES SEVEROS',
      color: 'alert-success',
      icon: 'fa fa-stethoscope',
      value: 0,
      class: 'col-md-4 col-sm-6'
    },
    {
      title: 'INSPECCIONES / FISCALIZACIONES / AUDITORÍAS',
      color: 'alert-warning',
      icon: 'fa fa-medkit',
      value: 0,
      class: 'col-md-4 col-sm-6'
    },
    {
      title: 'MULTAS AMBIENTALES',
      color: 'alert-danger',
      icon: 'fa fa-cubes',
      value: 0,
      class: 'col-md-4 col-sm-6'
    },
    {
      title: 'SANCIONES ADMINISTRATIVAS',
      color: 'alert-danger',
      icon: 'fa fa-ban',
      value: 0,
      class: 'col-md-4 col-sm-6'
    },
    {
      title: 'RECLAMO FORMAL COMUNIDAD',
      color: 'alert-warning',
      icon: 'fa fa-exclamation-triangle',
      value: 0,
      class: 'col-md-4 col-sm-6'
    },
    {
      title: 'RESIDUOS DOMICILIARIOS / HOMBRE DÍA (KG / HD)',
      color: 'alert-info',
      icon: 'fa fa-user',
      value: '0.0',
      class: 'col-md-4 col-sm-4'
    },
    {
      title: 'RESIDUOS PELIGROSOS / MAQUINA MES (KG / MAQ-MES)',
      color: 'alert-success',
      icon: 'fa fa-exchange',
      value: '0.0',
      class: 'col-md-4 col-sm-4'
    },
    {
      title: 'ESCOMBRO DE HORMIGÓN / HORMIGÓN VERTIDO (%)',
      color: 'alert-info',
      icon: 'fa fa-user',
      value: '0.0',
      class: 'col-md-4 col-sm-4'
    }
  ];



  constructor(private chartService: ChartService, private router: Router) {

  }


  ngOnInit() {
    this.chartService.getCantPedByCli().subscribe(resp => {
      this.data = resp.document; // Accede al array 'document' dentro del objeto de respuesta
      console.log(this.data);
      if (this.data != null) {
        for (let i = 0; i < this.data.length; i++) {
          this.dataidcliente.push(this.data[i].id_cliente);
          this.datacantidadpedidos.push(this.data[i].cantidad_pedidos);
        }
      }
      this.showGetCantPedByCli(this.dataidcliente, this.datacantidadpedidos);
    });



    //CHART PROMEDIO GASTO POR CLIENTE
    this.chartService.getPromGastoByCli().subscribe(resp => {
      let data = resp.document; // Accede al array 'document' dentro del objeto de respuesta
      let dataidcliente: any[] = [];
      let dataPromGasto: any[] = [];
      console.log(data);
      if (data != null) {
        for (let i = 0; i < data.length; i++) {
          dataidcliente.push(data[i].id_cliente);
          dataPromGasto.push(data[i].prom_total);
        }
      }
      this.showGetPromGastoByCli(dataidcliente, dataPromGasto);
      this.showGetTest(dataidcliente, dataPromGasto);
      this.showGetTest2(dataidcliente, dataPromGasto);
      this.showLineChart(dataidcliente, dataPromGasto);
      this.showHorizonChart(dataidcliente, dataPromGasto);
    });




  }

  goToList() {
    this.router.navigate(['/cliente-list'])
  }

  goToCreate() {
    this.router.navigate(['/cliente-create'])
  }

  showGetCantPedByCli(dataidcliente: any, datacantidadpedidos: any) {
    // Mostrar solo los primeros 5 elementos

    // Array de colores
    let colors = ['#2C3E50', '#2980B9', '#27AE60', '#F39C12', '#8E44AD'];


    console.log(dataidcliente);
    new Chart("CantPedByCli", {
      type: 'bar',
      data: {
        labels: dataidcliente,
        datasets: [{
          label: 'Cantidad de pedidos por cliente',
          data: datacantidadpedidos,
          backgroundColor: colors, // Asigna los colores a las barras
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }

  showGetPromGastoByCli(dataidcliente: any, dataPromGasto: any) {
    // Array de colores
    let colors = ['#2C3E50', '#2980B9', '#27AE60', '#F39C12', '#8E44AD'];

    console.log(dataidcliente);
    new Chart("PromGastoByCli", {
      type: 'line',
      data: {
        labels: dataidcliente.slice(0, 5),
        datasets: [{
          label: 'Promedio de gasto por cliente',
          data: dataPromGasto.slice(0, 5),
          backgroundColor: colors, // Asigna los colores a las barras
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }


  showGetTest(dataidcliente: any, dataPromGasto: any) {
    // Array de colores
    let colors = ['#2C3E50', '#2980B9', '#27AE60', '#F39C12', '#8E44AD'];


    new Chart("Test", {
      type: 'pie',
      data: {
        labels: dataidcliente.slice(0, 5),
        datasets: [{
          label: 'Promedio de gasto por cliente',
          data: dataPromGasto.slice(0, 5),
          backgroundColor: colors, // Asigna los colores a las barras
          borderWidth: 1
        }]
      },
      options: {
        responsive: false,
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }

  showGetTest2(dataidcliente: any, dataPromGasto: any) {
    // Array de colores
    let colors = ['#2C3E50', '#2980B9', '#27AE60', '#F39C12', '#8E44AD'];


    new Chart("Test2", {
      type: 'doughnut',
      data: {
        labels: dataidcliente.slice(0, 5),
        datasets: [{
          label: 'Promedio de gasto por cliente',
          data: dataPromGasto.slice(0, 5),
          backgroundColor: colors, // Asigna los colores a las barras
          borderWidth: 1
        }]
      },
      options: {
        responsive: false,
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }

    });
  }


  showLineChart(dataidcliente: any, dataPromGasto: any) {
    // Array de colores
    let colors = ['#2C3E50', '#2980B9', '#27AE60', '#F39C12', '#8E44AD'];

    new Chart("LineChart", {
      type: 'line',
      data: {
        labels: dataidcliente,
        datasets: [{
          label: 'Promedio de gasto por cliente',
          data: dataPromGasto,
          backgroundColor: colors, // Asigna los colores a las barras
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }


  showHorizonChart(dataidcliente: any, dataPromGasto: any) {
    // Array de colores
    let colors = ['#2C3E50', '#2980B9', '#27AE60', '#F39C12', '#8E44AD'];

    new Chart("HorizonChart", {
      type: 'line',
      data: {
        labels: dataidcliente,
        datasets: [{
          label: 'Promedio de gasto por cliente',
          data: dataPromGasto,
          backgroundColor: colors, // Asigna los colores a las barras
          borderWidth: 1
        }]
      },
      options: {
        indexAxis: 'y',
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each horizontal bar to be 2px wide
        elements: {
          bar: {
            borderWidth: 2,
          }
        },
        responsive: true,
        plugins: {
          legend: {
            position: 'right',
          },
          title: {
            display: true,
            text: 'Horizontal Chart'
          }
        }
      },
    });
  }











}


