import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { PedidosService } from '../cliente-create/pedidos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, NgForm } from '@angular/forms';
import { CommonModule } from '@angular/common';
import Swal from "sweetalert2"

@Component({
  selector: 'app-cliente-edit',
  standalone: true,
  imports: [MatInputModule, MatFormFieldModule, FormsModule, CommonModule],
  templateUrl: './cliente-edit.component.html',
  styleUrl: './cliente-edit.component.scss'
})
export class ClienteEditComponent implements OnInit {
  @ViewChild('editarPedidoForm') editarPedidoForm!: NgForm;
  @Output() enviarTitulo: EventEmitter<string> = new EventEmitter<string>();
  pedidoId: any;
  pedidos: any = {};


  constructor(
    private router: Router,
    public route: ActivatedRoute,
    private pedidosService: PedidosService) {
    this.enviarTitulo.emit("Editar Pedido");

  }

  ngOnInit(): void {

    //obtener id del pedido
    this.pedidoId = this.route
      .snapshot
      .paramMap
      .get('id');

    //obtener pedido por id
    this.pedidosService.getMethod('ventas/getPedidoId.php?id=' + this.pedidoId)
      .subscribe((resp: any) => {
        this.pedidos = resp.document;
        console.log(this.pedidos);


      })
  }

  editarPedido() {

    Swal.fire({
      title: "Deseas guardar los cambios?",
      showDenyButton: true,
      confirmButtonText: "Aceptar",
      denyButtonText: `Rechazar`
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {

        let formData = new FormData();
        formData.append('id_cliente', this.pedidos.id_cliente);
        formData.append('fecha_pedido', this.pedidos.fecha_pedido);
        formData.append('total', this.pedidos.total);
        formData.append('id_pedido', this.pedidos.id_pedido);

        this.pedidosService.postMethod('ventas/updatePedido.php', formData)
          .subscribe((res: any) => {
            console.log(res);
            Swal.fire("Pedido actualizado", "", "success");
            this.router.navigate(['/cliente-list']);
          }, (err: any) => {
            console.log("error", err);
          })

      } else if (result.isDenied) {
        Swal.fire("Los cambios fueron rechazados", "", "info");
      }
    });




  }

}
