import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { RouterOutlet, RouterModule, Router } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ClienteCreateComponent } from './cliente/cliente-create/cliente-create.component';
import { MatSidenav, MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { ChangeDetectorRef } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from './login/login.service';
import { MatExpansionModule } from '@angular/material/expansion';
import Swal from 'sweetalert2';
import { initFlowbite } from 'flowbite';



@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    RouterModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    ClienteCreateComponent,
    HttpClientModule,
    MatExpansionModule
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  shouldRun = /(^|.)(stackblitz|webcontainer).(io|com)$/.test(window.location.host);
  title = 'dashboard';
  tituloHijo!: string;

  @ViewChild(MatSidenav)
  sideNav!: MatSidenav;

  isLoggedIn!: boolean;

  panelOpenState = false;


  constructor(private observer: BreakpointObserver, private cd: ChangeDetectorRef, private router: Router, private logService: LoginService) {

  }

  ngOnInit() {
    initFlowbite();
    this.logService.isUserLogin.subscribe((res) => {
      this.isLoggedIn = res;
    })
  }

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 800px)']).subscribe((res) => {
      console.log(res);
      if (res.matches) {
        this.sideNav.mode = 'over';
        this.sideNav.close();
      } else {
        this.sideNav.mode = 'side';
        this.sideNav.open();
      }
      this.cd.detectChanges();
    });
  }

  deleteToken() {

    Swal.fire({
      title: "Cerrar sesión?",
      showCancelButton: true,
      confirmButtonText: "Cerrar",
      denyButtonText: `Cancelar`
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        localStorage.removeItem('token');
        this.router.navigateByUrl('/login');
        this.logService.isUserLogin.next(false);
        this.logService.isUserLogin.subscribe((res) => {
          this.isLoggedIn = res;
        });

      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });


  }

  recibirTitulo(titulo: any): void {
    this.tituloHijo = titulo;
  }


}
